1. 初始化一个版本库 git init
2. 把修改添加到暂存区 git add 文件名
3. 提交到版本库（分支）git commit -m "message"
4. 查看工作区和版本库的不同 git status
5. 查看某个文件修改前后的不同 git diff 文件名
6. 撤销文件的修改 git restore 文件名
7. 查看当前所有的版本记录 git log
8. 查看所有涉及版本变化命令的记录 git reflog
9. 移除工作区某个文件 rm 文件名
10. 移除版本库中某个文件 git rm 文件名
11. 回退版本 git reset --hard comID
